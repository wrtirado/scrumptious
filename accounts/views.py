from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LogInForm

# User Sign Up View Function
def signup(request):
	if request.method == "POST":
		form = SignUpForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			password_confirmation = form.cleaned_data['password_confirmation']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			
			if password == password_confirmation:
				# Create a new user with those values
				# and save it to a variable
				user = User.objects.create_user(
				    username,
   			        email=None,
				    password=password,
				    first_name=first_name,
				    last_name=last_name,
				)
				# Login the user with the user you just
				# created
				login(request, user)
				return redirect("recipe_list")
			else:
				form.add_error("password", "Passwords do not match")
	else:
		form = SignUpForm()
	context = {
		'form': form,
	}
	return render(request, 'accounts/signup.html', context)

# User Login Form View Function
def user_login(request):
	if request.method == "POST":
		form = LogInForm(request.POST)
		if form.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			
			user = authenticate(request, username=username, password=password)
			if user is not None:
				login(request, user)
				return redirect('recipe_list')
	else:
		form = LogInForm()
	context = {
		'form': form,
	}
	return render(request, 'accounts/login.html', context)

# User Logout View Function
def user_logout(request):
	logout(request)
	return redirect('recipe_list')