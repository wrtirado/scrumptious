from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# Create your views here.
# Full List of Recipes View Function
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)

# User Specific Recipes View Function
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, 'recipes/list.html', context)

# Specific Recipe Detail View Function
def show_recipe(request, id):
    #return HttpResponse("This view is working")
    recipe = Recipe.objects.get(id=id)

    context = {
        "recipe_object": recipe
    }

    return render(request, 'recipes/detail.html', context)

# Create View Function
@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form
    }
    return render(request, 'recipes/create.html', context)

# Edit View Function
def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    if request.method =="POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe_object": recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)